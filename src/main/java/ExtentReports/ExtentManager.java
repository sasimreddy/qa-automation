package ExtentReports;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;


public class ExtentManager {
	
	public Date date = new Date(); //Date 
	public static String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
    private static ExtentReports extent;
    private static String reportFileName = "Report-" + timeStamp +".html";;
    private static String reportFilepath = System.getProperty("user.dir") +"/src/test/resources/ExtentReports/";
    private static String reportFileLocation =  reportFilepath + reportFileName;
  
 
    public static ExtentReports getInstance() {
        if (extent == null)
            createInstance();
        return extent;
    }
 
    //Create an extent report instance
    public static ExtentReports createInstance() {
        String fileName = getReportPath(reportFilepath);
       
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(fileName);
		/*
		 * htmlReporter.config().setTestViewChartLocation(ChartLocation.BOTTOM);
		 * htmlReporter.config().setChartVisibilityOnOpen(true);
		 * htmlReporter.config().setTheme(Theme.DARK);
		 * htmlReporter.config().setDocumentTitle(reportFileName);
		 * htmlReporter.config().setEncoding("utf-8");
		 * htmlReporter.config().setReportName(reportFileName); htmlReporter.config().
		 * setTimeStampFormat("EEEE, MMMM dd, yyyy, hh:mm a '('zzz')'");
		 */
 
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
        
        
        //Set environment details
		extent.setSystemInfo("OS", "macOS Catalina");
		extent.setSystemInfo("AUT", "QA");
		htmlReporter.loadXMLConfig("./src/test/resources/extentconfig/ReportsConfig.xml");
 
        return extent;
    }
     
    //Create the report path
    private static String getReportPath (String path) {
    	File testDirectory = new File(path);
        if (!testDirectory.exists()) {
        	if (testDirectory.mkdir()) {
                System.out.println("Directory: " + path + " is created!" );
                return reportFileLocation;
            } else {
                System.out.println("Failed to create directory: " + path);
                return System.getProperty("user.dir");
            }
        } else {
            System.out.println("Directory already exists: " + path);
        }
		return reportFileLocation;
    }
 
}
