package ExtentReports;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.maven.shared.utils.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.barn2door.base.Page;

public class Utility extends Page{

	
	public static String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
	
	public static String getScreenshot(WebDriver driver, String screenShotName) {
		
		TakesScreenshot ts = (TakesScreenshot)driver;
		File src=ts.getScreenshotAs(OutputType.FILE);
		String path = System.getProperty("user.dir")+"/src/test/resources/Screenshots/"+screenShotName+ timeStamp +".png";
		File destination=new File(path);
		
		try {
			
			FileUtils.copyFile(src, destination);
		} catch(IOException e) {
			
			System.out.println("Capture Failed "+e.getMessage());
		}
		
		return path;
		
		
	}
}
