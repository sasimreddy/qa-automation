package com.barn2door.pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPageLocators {
	
	//Sign in Button
	@FindBy(xpath = "//a[contains(text(),'Sign in')]")
	public WebElement Signin;
	
	//Sign Up to Sell Button
	@FindBy(xpath = "//a[contains(text(),'Sign Up to Sell')]")
	public WebElement SignUptoSell;
	
	//Login
	@FindBy(xpath = "//input[@value='Login']")
	public WebElement Login;
	
	//Header Log in to your account
	@FindBy(xpath = "//div[@class='login-page']//h1")
	public WebElement LogIntoYourAccountHeader;
	
	//Sign Up to Buy Button
	@FindBy(xpath = "//a[contains(text(),'Sign Up to Buy')]")
	public WebElement SignUptoBuy;

	//------------------Sign Up to Buy & Sell---------------------//
	@FindBy(xpath = "//div[@class='buyer-signup']//h1")
	public WebElement SignUptoBuyFoodHeader;
	
	@FindBy(xpath = "//div[@class='signup-page']//h1")
	public WebElement SignUptoSellFoodHeader;
	
	//First Name
	@FindBy(xpath = "//input[@name='firstName']")
	public WebElement FirstName;
	
	//Last Name
	@FindBy(xpath = "//input[@name='lastName']")
	public WebElement LastName;
	
	//Phone Number
	@FindBy(xpath = "//input[@name='mobilePhone']")
	public WebElement PhoneNumber;
	
	//EMail Address
	@FindBy(xpath = "//input[@name='email']")
	public WebElement Email;
	
	//Password
	@FindBy(xpath = "//input[@name='password']")
	public WebElement Password;
	
	//Confirm Password
	@FindBy(xpath = "//input[@name='confirmPassword']")
	public WebElement ConfirmPassword;
	
	//ZipCode
	@FindBy(xpath = "//input[@name='postalCode']")
	public WebElement Zipcode;
	
	//Already Have Account Login
	@FindBy(xpath = "//p[@class='login-text']/a")
	public WebElement AlreadyHaveAccountLogin;
	
	//Sign Up Button
	@FindBy(xpath = "//input[@value='Sign Up']")
	public WebElement SignUpButton;
	
	//Delivery Area - Submit Button
	@FindBy(xpath = "//input[@value='Submit']")
	public WebElement DeliveryArea_Submit;
	
	//FirstName in Top Navigator
	@FindBy(xpath = "//li[@class='has-dropdown not-click']/a")
	public WebElement AfterSignUp_FirstName;
	
	//--------------------Sell-------------------------------------//
	//Next Step Button
	@FindBy(xpath = "//input[@value='Next Step']")
	public WebElement NextStep;
	
	//Edit Button Account Details
	@FindBy(xpath = "//div[@class='edit-step-container signup-form']//button[@class='edit-step']")
	public WebElement AccountDetailsEdit;
	
	//--------Billing Info-------//
	
	//Name on Card
	@FindBy(xpath = "//input[@id='nameOnCard']")
	public WebElement NameOnCard;
	
	//Name on Card
	@FindBy(xpath = "//input[@id='creditCard']")
	public WebElement CreditCardNumber;
	
	//Month
	@FindBy(xpath = "//select[@id='expMonth']/option[6]")
	public WebElement Month;
	
	//Year
	@FindBy(xpath = "//select[@id='expYear']/option[6]")
	public WebElement Year;
	
	//Year
	@FindBy(xpath = "//input[@id='cvv']")
	public WebElement CVV;
	
	//------Subscription Plan-------//
	
	//Subscription Plan Hobby
	@FindBy(xpath = "//div[@class='subscription-item']//span[contains(text(),'Hobby')]")
	public WebElement Subs_Plan_Hobby;
	
	//Terms and Conditions
	@FindBy(xpath = "//input[@id='agreeTerms']")
	public WebElement TermsCheckBox;
	
	//Sign Up
	@FindBy(xpath = "//button[contains(text(),'Sign Up')]")
	public WebElement SignUp;
	
	//DashBoard
	@FindBy(xpath = "//div[@class='sub-nav']//a[contains(text(),'Dashboard')]")
	public WebElement DashBoard;
	
	
	
	
	
}
