package com.barn2door.pages.actions;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.barn2door.base.Page;
import com.barn2door.base.Variables;
import com.barn2door.pages.locators.SignInPageLocators;

import ExtentReports.ExtentTestManager;

public class SignUptoSell extends Page {
	
	public SignInPageLocators signinpage;
	private static Logger logger = Logger.getLogger(SignUptoBuy.class);
	String SignUpoToSellFood_Header = "Sign Up To Sell Food"; 
	String AfterSignUp_FirstName;
	
	public SignUptoSell() {
		
		this.signinpage = new SignInPageLocators();
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver,20);
		PageFactory.initElements(factory, this.signinpage);
	}
	
	
	
		//Click on Sign Up to Sell
		public void clickSignUptoSell() throws InterruptedException {
			
			wait.until(ExpectedConditions.elementToBeClickable(signinpage.Login)); //Wait till login button is clickable
			
				Assert.assertEquals(signinpage.LogIntoYourAccountHeader.getText(), "Log In To Your Account");
					logger.info("Asserted Passed Matched Header");
						ExtentTestManager.getTest().log(Status.PASS, "Log In To Your Account Text Matched");
						
					Thread.sleep(2000);
						signinpage.SignUptoSell.click();
							logger.info("Clicked on Sign Up to sell");
								ExtentTestManager.getTest().log(Status.PASS, "Clicked on Sign Up to sell");
							
							wait.until(ExpectedConditions.elementToBeClickable(signinpage.AlreadyHaveAccountLogin));
							
							//Header Comparision
							String Header = signinpage.SignUptoSellFoodHeader.getText();
								logger.info(Header);
									Assert.assertEquals(Header.toLowerCase(), SignUpoToSellFood_Header.toLowerCase());
										logger.info("Header Matched");
											ExtentTestManager.getTest().log(Status.PASS, "Sign Up To Sell Food Header Matched");
											Thread.sleep(2000);
			
		}
		
		//Account Details are common for both buy and sell.
		
		//Billing Info
		public void BillingInfo() {
			
			//Click on Next Step
			signinpage.NextStep.click();
				logger.info("Clicked on Next Step");
					ExtentTestManager.getTest().log(Status.PASS, "Clicked on Next Step");

			
			wait.until(ExpectedConditions.elementToBeClickable(signinpage.NextStep)); //Wait till Next Step button is clickable
			
			//Enter CC Name
			signinpage.NameOnCard.sendKeys(Variables.NameOnCard);
				logger.info("Entered Name on the card");
					ExtentTestManager.getTest().log(Status.PASS, "Entered Credit Card Name");

			
			//Enter CC Number
			signinpage.CreditCardNumber.sendKeys(Variables.CreditCardNumber);
				logger.info("Entered Credit Card No");
					ExtentTestManager.getTest().log(Status.PASS, "Entered Credit Card Number");

			
			//Select Month
			signinpage.Month.click();
				logger.info("Selected Month");
					ExtentTestManager.getTest().log(Status.PASS, "Selected  Month");


			//Select Year
			signinpage.Year.click();
				logger.info("Selected Year");
					ExtentTestManager.getTest().log(Status.PASS, "Selected Year");

			
			//Enter CVV
			signinpage.CVV.sendKeys(Variables.CVV);
				logger.info("Entered CVV");
					ExtentTestManager.getTest().log(Status.PASS, "Entered CVV");

			
			//Zipcode
			signinpage.Zipcode.sendKeys(Variables.Zipcode);
				logger.info("Entered Zipcode: "+Variables.Zipcode);
					ExtentTestManager.getTest().log(Status.PASS, "Entered Zip Code");

				
			//Click on Next Step
			signinpage.NextStep.click();
				logger.info("Clicked on Next Step");
			
		}
		
		public void SubscriptionPlan() {
			
			wait.until(ExpectedConditions.elementToBeClickable(signinpage.Subs_Plan_Hobby)); //Wait till Checkbox button is clickable
			
			//Click on Hobby Subscription
			signinpage.Subs_Plan_Hobby.click();
				logger.info("Clicked on Hobby");
					ExtentTestManager.getTest().log(Status.PASS, "Selected Hobby Subscription Plan");

				
			//Click on Terms and Conditions Check Box
			signinpage.TermsCheckBox.click();
				logger.info("Clicked on Check Box");	
					ExtentTestManager.getTest().log(Status.PASS, "Selected Terms and Conditions");

				
			//Sign Up
			signinpage.SignUp.click();
				logger.info("Clicked on Sign Up");
					ExtentTestManager.getTest().log(Status.PASS, "Sign Up");

				
			//After Successful Signup wait for sidenavs DASHBOARD to load
			wait.until(ExpectedConditions.elementToBeClickable(signinpage.DashBoard)); //Wait till DashBoard button is clickable
	
			logger.info("Successfully Signed to be a Seller");
				ExtentTestManager.getTest().log(Status.PASS, "Successfully Signed In to be a seller");

			
		}
}
