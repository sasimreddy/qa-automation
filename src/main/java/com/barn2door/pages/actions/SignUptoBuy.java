package com.barn2door.pages.actions;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.barn2door.base.Page;
import com.barn2door.base.Variables;
import com.barn2door.pages.locators.SignInPageLocators;

import ExtentReports.ExtentTestManager;


public class SignUptoBuy extends Page {
	
	public SignInPageLocators signinpage;
	private static Logger logger = Logger.getLogger(SignUptoBuy.class);
	String SignUpoToBuyFood_Header = "Sign up to Buy Food"; 
	String AfterSignUp_FirstName;
	
	
	public SignUptoBuy() {
		
		this.signinpage = new SignInPageLocators();
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver,20);
		PageFactory.initElements(factory, this.signinpage);
	}

	//Click on SignIn
	public void clickSignin() {
		signinpage.Signin.click();
			logger.info("Clicked on Sign In");
				ExtentTestManager.getTest().log(Status.PASS, "Clicked on Sign In");
		
	}
	
	//Click on Sign Up to Buy
	public void clickSignUptoBuy() throws InterruptedException {
		
		wait.until(ExpectedConditions.elementToBeClickable(signinpage.Login)); //Wait till login button is clickable
		
		Assert.assertEquals(signinpage.LogIntoYourAccountHeader.getText(), "Log In To Your Account");
			ExtentTestManager.getTest().log(Status.PASS, "Log In To Your Account Text Matched");
				Thread.sleep(1000);
			
			signinpage.SignUptoBuy.click();
				logger.info("Clicked on Sign Up to Buy");
					ExtentTestManager.getTest().log(Status.PASS, "Clicked on Sign Up to Buy");
			
			wait.until(ExpectedConditions.elementToBeClickable(signinpage.AlreadyHaveAccountLogin));
			
			//Header Comparision
			String Header = signinpage.SignUptoBuyFoodHeader.getText();
				logger.info(Header);
				
				Assert.assertEquals(Header.toLowerCase(), SignUpoToBuyFood_Header.toLowerCase());
					logger.info("Sign Up To Buy Food Text Matched");
						ExtentTestManager.getTest().log(Status.PASS, "Sign Up To Buy Food Text Matched");
						
					
	}
	
	//Enter Account Info
	public void AccountDetails() throws InterruptedException {
		
		//FirstName
		signinpage.FirstName.sendKeys(Variables.FirstName);
			logger.info("Entered First Name: "+Variables.FirstName);
				ExtentTestManager.getTest().log(Status.PASS, "Entered First Name");
		
		//LastName
		signinpage.LastName.sendKeys(Variables.LastName);
			logger.info("Entered Last Name: "+Variables.LastName);
				ExtentTestManager.getTest().log(Status.PASS, "Entered Last Name");
			
			Thread.sleep(1000);
			
		//PhoneNumber
		signinpage.PhoneNumber.sendKeys(Variables.PhoneNumber);
			logger.info("Entered Phone Number: "+Variables.PhoneNumber);
				ExtentTestManager.getTest().log(Status.PASS, "Entered Phone Number");
			
		//Email
		signinpage.Email.sendKeys(Variables.Email);
			logger.info("Entered Email: "+Variables.Email);
				signinpage.Email.sendKeys(Keys.TAB);
					ExtentTestManager.getTest().log(Status.PASS, "Entered Email");
			
			Thread.sleep(2000);

		//Password
		signinpage.Password.sendKeys(Variables.Password);
			logger.info("Entered Password: "+Variables.Password);
				ExtentTestManager.getTest().log(Status.PASS, "Entered Password");
			
		//Confirm Password
		signinpage.ConfirmPassword.sendKeys(Variables.Password);
			logger.info("Entered Confirm Password: "+Variables.Password);
				ExtentTestManager.getTest().log(Status.PASS, "Entered Confirm Password");
			
		//Zipcode
		signinpage.Zipcode.sendKeys(Variables.Zipcode);
			logger.info("Entered Zipcode: "+Variables.Zipcode);
				ExtentTestManager.getTest().log(Status.PASS, "Entered Zip Code");
			
	}
	
	public void AccountDetails_SignUp() {
			
		//Click on SignUp
		signinpage.SignUpButton.click();
			logger.info("Clicked on SignUp");
				ExtentTestManager.getTest().log(Status.PASS, "Clicked on SignUp");
			
			wait.until(ExpectedConditions.elementToBeClickable(signinpage.DeliveryArea_Submit)); // Wait till Delivery Area Submit button is clickable
			
			//Compare Name (HI, FirstName)
			AfterSignUp_FirstName = "Hi, "+Variables.FirstName.toUpperCase();
				logger.info(signinpage.AfterSignUp_FirstName.getText());
					Assert.assertEquals(signinpage.AfterSignUp_FirstName.getText(), AfterSignUp_FirstName);
						logger.info("Name Matched");
							ExtentTestManager.getTest().log(Status.PASS, "Name Matched");
					
				
	}
	
}
