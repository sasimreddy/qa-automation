package com.barn2door.base;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Parameters;
import com.aventstack.extentreports.Status;
import ExtentReports.ExtentTestManager;



public class Page {
	
  
	public static WebDriver driver; //Driver instance
	//public static ExcelReader excel = new ExcelReader(System.getProperty("user.dir") + "\\src\\test\\resources\\excel\\testdata.xlsx");
	public static WebDriverWait wait; //WebDriver wait
	public DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy"); //Date Format
	public static String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
	public Calendar now = Calendar.getInstance(); // Calendar instance
	public Date date = new Date(); //Date 
	public static Actions actions; //Actions
	public static JavascriptExecutor jse; //Java Script Executor
	private static Logger logger = Logger.getLogger(Page.class);
  
	@Parameters({"Browser","Url"}) //Parameters passed through xml
	public static void initConfiguration(String Browser, String Url) {
		
		try {
			
			System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"DeleteThisLog"); //Stops printing the browser log files in console and creates a txt file called "DeleteThisLog"		 
			

		} catch (Exception e) {
			logger.info("Error....." + e.getStackTrace());
		}
			
		
		PropertyConfigurator.configure("log4j.properties");
		//Firefox
		if(Browser.equals("firefox")) {
			
			//Path to geckodriver
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"/src/test/resources/executables/geckodriver");

			driver = new FirefoxDriver();
				ExtentTestManager.getTest().log(Status.INFO, "Launching Firefox");
					logger.info("Launching Firefox");
			
		}
		
		//Chrome
		else if(Browser.equals("chrome")) {
			
			//Path to Chrome Driver
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/src/test/resources/executables/chromedriver");
			
			ChromeOptions chromeOptions = new ChromeOptions();
	 			chromeOptions.addArguments("--start-maximized");
	 				driver = new ChromeDriver(chromeOptions);
	 					ExtentTestManager.getTest().log(Status.INFO, "Launching Chrome");
	 						logger.info("Launching Chrome");
			
	 		
		}
		
		//Safari 
		else if(Browser.equals("safari")){

			driver = new SafariDriver();
				ExtentTestManager.getTest().log(Status.INFO, "Launching Safari");
					logger.info("Launching Safari");
		}
		
		//Defaults to Safari
		else {
			
			driver = new SafariDriver();
				ExtentTestManager.getTest().log(Status.INFO, "Launching Safari");
					logger.info("Browser Type Invalid, Launching Safari as Browser Choice");
						logger.info("Launching Safari");
		}
		
		wait = new WebDriverWait(driver, 15);
			actions = new Actions(driver);
				jse = (JavascriptExecutor)driver;
					driver.get(Url);
							driver.manage().window().maximize();
								ExtentTestManager.getTest().log(Status.INFO, "Browser Launched Successfully");
							
	
	}
	

	  public static void quitBrowser(){
	  
	  driver.quit();
	  
	  }
	 
	

}
