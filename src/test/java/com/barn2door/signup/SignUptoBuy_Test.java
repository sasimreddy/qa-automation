package com.barn2door.signup;

import org.testng.annotations.*;
import com.barn2door.base.Page;
import com.barn2door.pages.actions.SignUptoBuy;


public class SignUptoBuy_Test {
		
	@Test
	@Parameters({"Browser","Url"})
	public void SignUp_ToBuy(String Browser, String Url) throws InterruptedException {
		
		Page.initConfiguration(Browser, Url);
		SignUptoBuy signup_buy = new SignUptoBuy();
		signup_buy.clickSignin();
		signup_buy.clickSignUptoBuy();
		RandomInfoGenerator.Info();
		signup_buy.AccountDetails();
		signup_buy.AccountDetails_SignUp();
	}
	
	
	
	public void quitBrowser(){
		
	//Page.quitBrowser();	  
		  
	}

}
