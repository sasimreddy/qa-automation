package com.barn2door.signup;

import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Random;

import org.apache.log4j.Logger;

import com.arakelian.faker.model.Address;
import com.arakelian.faker.model.Person;
import com.arakelian.faker.service.RandomAddress;
import com.arakelian.faker.service.RandomPerson;
import com.barn2door.base.Variables;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;

public class RandomInfoGenerator {


 static Random random = new Random();
 private static Logger logger = Logger.getLogger(RandomInfoGenerator.class);
	
 public static void Info()  {
		
		Person person = RandomPerson.get().next();
		Address address = RandomAddress.get().next();
		
		//FirstName
		Variables.FirstName = person.getFirstName();
			logger.info("FirstName: "+Variables.FirstName);
			
		//LastName
		Variables.LastName = person.getLastName();
			logger.info("LastName: "+Variables.LastName);
	
		//PhoneNumber
		int num1 = (random.nextInt(7) + 1) * 100 + (random.nextInt(8) * 10) + random.nextInt(8);
        	int num2 = random.nextInt(743);
        		int num3 = random.nextInt(10000);
        			DecimalFormat df3 = new DecimalFormat("000"); // 3 zeros
        				DecimalFormat df4 = new DecimalFormat("0000"); // 4 zeros
        					Variables.PhoneNumber = df3.format(num1) + "-" + df3.format(num2) + "-" + df4.format(num3);
		logger.info("Phone Number: "+Variables.PhoneNumber);
		
		//Email Address
		FakeValuesService fakeValuesService = new FakeValuesService(new Locale("en-US"), new RandomService());	 
			Variables.Email = fakeValuesService.bothify("????##@gmail.com");
		logger.info("Email Address: "+Variables.Email);
		
		//Password
		Variables.Password = String.valueOf(generatePassword(8));
			logger.info("Password: "+Variables.Password);
			
		//ZipCode
		Variables.Zipcode = address.getPostalCode();
			logger.info("Zip Code: "+Variables.Zipcode);
		
//		System.out.println(address.getStreet());
//		System.out.println(address.getCity());
//		System.out.println(address.getState());
//		System.out.println(address.getPostalCode()
//				);
		   
		    
	}
	
	private static char[] generatePassword(int a) {
	      String capitalCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	      String lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
	      String specialCharacters = "!@#$";
	      String numbers = "1234567890";
	      String combinedChars = capitalCaseLetters + lowerCaseLetters + specialCharacters + numbers;
	      
	      char[] password = new char[a];

	      password[0] = lowerCaseLetters.charAt(random.nextInt(lowerCaseLetters.length()));
	      password[1] = capitalCaseLetters.charAt(random.nextInt(capitalCaseLetters.length()));
	      password[2] = specialCharacters.charAt(random.nextInt(specialCharacters.length()));
	      password[3] = numbers.charAt(random.nextInt(numbers.length()));
	   
	      for(int i = 4; i< a ; i++) {
	         password[i] = combinedChars.charAt(random.nextInt(combinedChars.length()));
	      }
	      return password;
	   }

}
