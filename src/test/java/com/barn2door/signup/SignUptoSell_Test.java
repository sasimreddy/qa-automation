package com.barn2door.signup;

import org.testng.annotations.*;
import com.barn2door.base.Page;
import com.barn2door.pages.actions.SignUptoBuy;
import com.barn2door.pages.actions.SignUptoSell;


public class SignUptoSell_Test {
	
	
	@Test
	@Parameters({"Browser","Url"})
	public void SignUp_ToSell(String Browser, String Url) throws InterruptedException {
		
		Page.initConfiguration(Browser, Url);
		SignUptoBuy signup_buy = new SignUptoBuy(); 
		SignUptoSell signup_sell = new SignUptoSell();
		signup_buy.clickSignin(); //Click on Signin
		signup_sell.clickSignUptoSell(); //Click on SignUptoSell
		RandomInfoGenerator.Info(); // Runs Random Info Generator 
		signup_buy.AccountDetails(); // Enters All the Account Details
		signup_sell.BillingInfo(); // Enters Details in Billing Info
		signup_sell.SubscriptionPlan(); //Subscription Plan
		
	

		
	}

	
	@AfterSuite
	public void quitBrowser(){
	
	//Page.quitBrowser();	  
		  
	}

}
